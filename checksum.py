#!/usr/bin/python3
import zlib
import os

if __name__ == '__main__':
    for file in os.listdir("./resources"):
        with open("./resources/" + file, "rb") as b:
            print(file)
            print("CRC32: " + str(zlib.crc32(b.read())))
            print("SIZE: " + str(os.path.getsize("./resources/" + file)))
            print("\n")
